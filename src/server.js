const crypto = require('crypto')
const queryString = require('querystring')
const axios = require('axios')
const util = require('util')


const SECRET_KEY = '<API-SECRET-HERE>';
const API_KEY = '<API-KEY-HERE>';

const BASE_URL = 'http://127.0.0.1:3001/api/v1/secured/otc-order'

class TradingPairs {
	static BTC_USD = 'BTC_USD'
	static BTC_USD = 'BTC_USD'
	static ETH_USD = 'ETH_USD'
	static USDT_USD = 'USDT_USD'
	static USDC_USD = 'USDC_USD'
	static XRP_USD = 'XRP_USD'
	static ADA_USD = 'ADA_USD'
	static EOS_USD = 'EOS_USD'
	static XTZ_USD = 'XTZ_USD'
	static XLM_USD = 'XLM_USD'
	static BCH_USD = 'BCH_USD'
	static PAX_USD = 'PAX_USD'
	static LEO_USD = 'LEO_USD'
	static ZEC_USD = 'ZEC_USD'
	static BSV_USD = 'BSV_USD'
	static BTT_USD = 'BTT_USD'
	static LTC_USD = 'LTC_USD'
	static TRX_USD = 'TRX_USD'
}

class TradeSides {
	static BUY = 'BUY'
	static SELL = 'SELL'
} class Status { static COMPLETED = 'COMPLETED'
	static FAILED = 'FAILED'
	static CANCELLED = 'CANCELLED'
	static PENDING = 'PENDING'
}

class OrderType {
	static MARKET_ORDER = 'MARKET_ORDER'
}

/* 
	Request config is a template for Http request data like method, endPoint, timestamp, payload.
	it uses default values given below
*/

class RequestConfig {
	method = 'GET'
	endPoint = '/'
	timestamp = new Date().getTime()
	payload = {}

	constructor(method, endPoint, payload, timestamp) {
		this.method = method || 'GET'
		this.endPoint = endPoint || '/'
		this.payload = payload || {}
		this.timestamp = timestamp || new Date().getTime()
	}
}

// end of configuration variables

/* 
	This below function is responsible for converting payload to sorted (according to key) query params
	example:

		{
			user: "clientUser",
			role: "admin"
		}

		would be converted to

		role=admin&user=clientUser

		** This sorting of payload is required to pass the authentication
*/
const objectToSortedQueryString = async (payload) => {
	const queryStringPayload = new URLSearchParams(queryString.stringify(payload))
	queryStringPayload.sort()

	return queryStringPayload.toString()
}

const getHmacSignature = async (payload, secretKey, timestamp) => {
	const payloadWithTimestamp = { ...payload, timestamp }

	const queryStringPayload = await objectToSortedQueryString(payloadWithTimestamp)

	const signature = crypto
		.createHmac('sha256', secretKey)
		.update(queryStringPayload)
		.digest('hex')
	return signature
}

/* 
	Basic structure class for Request processing. like putting headers with correct signature
*/
class Request {
	static headers = {}

	static setHeader = async (key, value) => {
		this.headers[key] = value
	}

	static setAuthorizationHeader = async (payload, timestamp) => {
		const signature = await getHmacSignature(payload, SECRET_KEY, timestamp)

		await Request.setHeader('timestamp', timestamp)
		await Request.setHeader('signature', signature)
		await Request.setHeader('apikey', API_KEY)
	}

	static get = async (requestConfig) => {
		await Request.setAuthorizationHeader(requestConfig.payload, requestConfig.timestamp)
		return await axios.get(BASE_URL + requestConfig.endPoint, {
			params: requestConfig.payload,
			headers: this.headers,
		})
	}

	static post = async (requestConfig) => {
		await Request.setAuthorizationHeader(requestConfig.payload, requestConfig.timestamp)
		return await axios.post(BASE_URL + requestConfig.endPoint, requestConfig.payload, {
			headers: this.headers,
		})
	}

	static request = async (requestConfig) => {
		if (requestConfig.method == 'GET') return await Request.get(requestConfig)
		else if (requestConfig.method == 'POST') return await Request.post(requestConfig)
	}
}

/*
	Test preparing class for requesting endpoints.
*/

class TestApiRequest {
	static getAllOrders = async () => {
		const requestConfig = new RequestConfig('GET', '/', {
			page: 1,
			tradeSide: TradeSides.BUY,
			status: Status.FAILED,
			tradingPair: TradingPairs.BTC_USD,
			// startDate: '2020-10-08',
			// endDate: '2020-10-08',
		})

		return await Request.request(requestConfig)
	}

	static getOrderById = async (orderId) => {
		const requestConfig = new RequestConfig('GET', `/${orderId}`, {})

		return await Request.request(requestConfig)
	}

	static getPrice = async () => {
		const requestConfig = new RequestConfig('GET', '/price', {
			side: TradeSides.BUY,
			quantity: 0.00001,
			tradingPair: TradingPairs.BTC_USD,
		})

		return await Request.request(requestConfig)
	}

	static createOrder = async () => {
		const res = await TestApiRequest.getPrice()

		const { requestId } = res.data.data

		const requestConfig = new RequestConfig('POST', '/', {
			requestId,
			orderType: OrderType.MARKET_ORDER,
		})

		return Request.request(requestConfig)
	}
}


// This function is used for testing the above API. Because all API are promise based 
// so it is easy to handle promise in a async function.
const testApi = async () => {
	try {
		let response = null

		try {
			response = await TestApiRequest.getAllOrders()
			console.log(response)
		}catch (e){
			console.log({response: e.response})
		}



		try {
			response = await TestApiRequest.getOrderById('<order-id>')
			console.log(response)
		}catch (e){
			console.log({response: e.response})
		}

		try {
			response = await TestApiRequest.createOrder()
			console.log(response)
		}catch (e){
			console.log({response: e.response})
		}


	} catch(e) {
		console.log(e);
	}
}

// Staring point
testApi()