# Public Rest API for Delchain (2020-10-16)
# General API Information
* The base endpoint is: **https://uatwa.delchain.net/api/v1/** for testing (UAT)
and **https://be.wa.delchain.net/api/v1/** for Production
* All endpoints return either a JSON object or array.
* HTTP `5XX` return codes are used for internal errors; the issue is on
  Server's side.
  It is important to **NOT** treat this as a failure operation; the execution status is
  **UNKNOWN** and could have been a success.
* HTTP `4XX` returns message as error message 
* Any endpoint can return an ERROR; the error payload is as follows:
```json5
{
  "message": "error message.",
  "data": {
      // Optional payload in case of additional information
  }
}
```

* For `GET` endpoints, parameters must be sent as `query` parameters.
* For `POST`, `PUT` endpoints, the parameters must be sent
  in the `body` with content type
  `application/json`.
* All authenticated api's are requested to accept timestamp parameters apart from their default params

# Endpoint security
## APIKEY
* API-keys are passed into the Rest API via the `apikey`
  header.
* All the parameters **are case sensitive**.
* All Apis are secured and can only be accessed with a valid `apikey` , `timestamp` and  hmac  `signature` with  in the headers

## HMAC / Authentication 
* Endpoints require an additional header, `signature`  to be
  sent for `GET`, `PUT` and `POST`.
* Endpoints use `HMAC SHA256` signatures. The `HMAC SHA256 signature` is a keyed `HMAC SHA256` operation.
  Use your `secretKey` as the key and `payload` as the value for the HMAC operation.
* In case of GET `payload` is defined as all the parameters sent in `queryString`, 
 arranged in alphabetic order and In case of POST and PUT, all the parameters in `body` converted
 to a string much like queryString in alphabetic order.
* Working example to calculate hmac is given below.


## Timing security
* An endpoint also requires a header, `timestamp`, to be sent which
  should be the epochtime( in seconds ) of when the request was created and sent.
* Any authenticated request reaching server with a difference of more than 5 secs, will be rejected.

  

## Working Examples 
### Lets do a  GET /api/v1/secured/otc-order.
Here is a step-by-step example of how to send a valid payload while hitting the API with 
above endpoint with given apiKey and secretKey as default value.

Key | Value
------------ | ------------
apiKey | 31c261d5-a095-4770-bc15-9ff55e5aa253
secretKey | b64de9f4-c3b2-4721-a1b7-5d17825a3ebb

## In the header of the request
Key | Value
------------ | ------------
apikey | 31c261d5-a095-4770-bc15-9ff55e5aa253
timestamp | 1602853085769 
signature | 4774d3728460f3380c761ec1bed383bf61e01ef41a717631de6a0bad39b9114f 

##### In the query param:
Parameter | Value | Required
------------ | ------------ | --------
tradeSide | BUY | OPTIONAL 
status | FAILED | OPTIONAL



### Note : timestamp is also required for hmac calculation (though it is sent via headers)

In order to calculate the payload for above parameters we have to convert the key value pairs in query parameters with value of keys in ascending order

* **payload:**  status=FAILED&timestamp=1602853085769&tradeSide=BUY
* **HMAC SHA256 signature:**
  
    ```
    hmac: aad298d5abf9dc021943ef22c2b6a971c65ff784aad9e9541835215e01845eca
    ```
* you can use this online tool for hmac calculation https://www.freeformatter.com/hmac-generator.html 



# API Endpoints

## Terminology
* `apikey` refers to the api_key provided to you from delchain
* `timestamp` refers to epoch time in seconds ( integer )



## Authenticated endpoints.

**Note: ** Please see the **possible values** section for all possible values of any param.

##### Requires timestamp and hmac 
### Orders Endpoints
#### 1. Get all orders
```
GET /api/v1/secured/otc-order
```
**Request GET Params**

| Name        | Required |
| ----------- | -------- |
| page        | no       |
| sortWith    | no       |
| sortBy      | no       |
| tradingPair | no       |
| startDate   | no       |
| endDate     | no       |
| tradeSide   | no       |
| status      | no       |

**Response**
```json5
{
  "message": "success or error message",
  data: {
     orders: [{
    orderId: 'KGJ9BNT2',
    tradingPair: 'ADA_USD',
    status: 'COMPLETED',
    side: 'BUY',
    ccy1: 0.002,
    ccy2: 0,
    usdEquivalent: 0,
    price: 0.11,
    type: 'MARKET_ORDER',
    client: {
      id: 2,
      createdAt: '2020-10-08T11:49:18.983Z',
      updatedAt: '2020-10-08T11:49:18.983Z'
    },
    clientName: 'client2',
    apiKeyId: 4,
    clientId: '123232231',
    createdAt: '2020-10-21T10:31:57.254Z',
    updatedAt: '2020-10-21T10:32:06.025Z'
  },...],
  count: 2
  }
}
```



#### 2. Get One Order 

```
GET /api/v1/secured/otc-order/:orderId
```
**Response**

```json5
{
  success: true,
  message: 'Successful',
  data: {
    orderId: 'KGJ9BNT2',
    tradingPair: 'ADA_USD',
    status: 'COMPLETED',
    side: 'BUY',
    ccy1: 0.002,
    ccy2: 0,
    usdEquivalent: 0,
    price: 0.11,
    type: 'MARKET_ORDER',
    client: {
      id: 2,
      createdAt: '2020-10-08T11:49:18.983Z',
      updatedAt: '2020-10-08T11:49:18.983Z'
    },
    clientName: 'client2',
    apiKeyId: 4,
    clientId: '123232231',
    createdAt: '2020-10-21T10:31:57.254Z',
    updatedAt: '2020-10-21T10:32:06.025Z'
  },
  httpCode: 200
}
```

#### 3. Get Price
```
GET /api/v1/secured/otc-order/price
```
**Request Get Params**

| Name        | Required |
| ----------- | -------- |
| tradingPair | yes      |
| quantity    | yes      |
| side        | yes      |

**Response**

```json5
{
  "message": "success or error message",
  "data": {
   "requestId": "eyJ0cmFkaW5nUGFpciI6IkJUQ19VU0QiLCJzaWRlIjoiQlVZIiwiY2xpZW50SWQiOiIxMjMyMzIyMzEiLCJjY3kxIjowLjAwMiwicXVvdGVJZCI6NjIsImNjeTIiOjIyLjg1NTcxMDAwMDAwMDAwMiwidXNkRXF1aXZhbGVudCI6MjIuODU1NzEwMDAwMDAwMDAyLCJmdWxmaWxsbWVudFBhcnR5QXR0cmlidXRlcyI6eyJiYXNlQ3VycmVuY3kiOiJCVEMiLCJiYXNlQ3VycmVuY3lTaXplIjowLjAwMiwiY29zdCI6MjIuNzQyLCJjb3N0Q3VycmVuY3kiOiJVU0QiLCJjb3VudGVycGFydHlBdXRvU2V0dGxlcyI6dHJ1ZSwiY291bnRlcnBhcnR5RnVsbHlTZXR0bGVkQXQiOm51bGwsImV4cGlyeSI6IjIwMjAtMTAtMTZUMTM6Mjk6MTQuNjk0NzEwKzAwOjAwIiwiZmlsbGVkIjpmYWxzZSwiaGFzaElkIjoiUTQ0OTg4MjEiLCJpZCI6Nzc2MTk0NTY1Niwib3JkZXJJZCI6bnVsbCwicHJpY2UiOjExMzcxLCJwcm9jZWVkcyI6MC4wMDIsInByb2NlZWRzQ3VycmVuY3kiOiJCVEMiLCJxdW90ZUN1cnJlbmN5IjoiVVNEIiwicXVvdGVDdXJyZW5jeVNpemUiOm51bGwsInF1b3RlZEF0IjoiMjAyMC0xMC0xNlQxMzoyODo1OS42OTQ3MTArMDA6MDAiLCJyZXF1ZXN0ZWRBdCI6IjIwMjAtMTAtMTZUMTM6Mjg6NTguODc3MTc1KzAwOjAwIiwic2V0dGxlZEltbWVkaWF0ZWx5Ijp0cnVlLCJzZXR0bGVtZW50VGltZSI6IjIwMjAtMTAtMTZUMTM6Mjg6NTguODc3MTQ3KzAwOjAwIiwic2lkZSI6ImJ1eSIsInN0YXRlIjoibm9uZSIsInRvdGFsRGVmZXJDb3N0UGFpZCI6MCwidG90YWxEZWZlclByb2NlZWRzUGFpZCI6MCwidW5zZXR0bGVkQ29zdCI6MCwidW5zZXR0bGVkUHJvY2VlZHMiOjAsInVzZXJGdWxseVNldHRsZWRBdCI6bnVsbH0sImZlZSI6MC4xMTM3MSwicHJpY2VCZWZvcmVGZWUiOjIyLjc0MiwiZnVsZmlsbG1lbnRQYXJ0eSI6IkZUWCJ9",
    "tradingPair": "BTC_USD",
    "side": "BUY",
    "clientId": "123232231",
    "expiry": 1602854952694,
    "ccy1": 0.002,
    "ccy2": 22.85571
  }
}
```

#### 4. Create Order
```
POST /api/v1/secured/otc-order
```
**Request Body**

| Name      | Required |
| --------- | -------- |
| requestId | yes      |
| orderType | yes      |

**Response**
```json5
{
  message: 'success or error message'
  data: {
    orderId: 'KGJ9BNT2',
    tradingPair: 'ADA_USD',
    status: 'COMPLETED',
    side: 'BUY',
    ccy1: 0.002,
    ccy2: 0,
    usdEquivalent: 0,
    price: 0.11,
    type: 'MARKET_ORDER',
    client: {
      id: 2,
      createdAt: '2020-10-08T11:49:18.983Z',
      updatedAt: '2020-10-08T11:49:18.983Z'
    },
    clientName: 'client2',
    apiKeyId: 4,
    clientId: '123232231',
    createdAt: '2020-10-21T10:31:57.254Z',
    updatedAt: '2020-10-21T10:32:06.025Z'
  },
  httpCode: 200
}

```



### Possible Values 

- **page**

    page number for pagination start from 1.

- **sortWith**

    - tradingPair
    - status
    - side
    - type
    - fulfillmentParty
    - ccy1
    - ccy2
    - createdAt
    - updatedAt
    - id
    - creator
    - client

- **sortBy**

    - DESC
    - ASC

- **tradingPair**

    - BTC_USD
    - ETH_USD
    - XRP_USD
    - ADA_USD
    - EOS_USD
    - XTZ_USD
    - XLM_USD
    - BCH_USD
    - PAX_USD
    - LEO_USD
    - ZEC_USD
    - BSV_USD
    - BTT_USD
    - LTC_USD
    - TRX_USD
    
- **startDate**

    start date filter on orders in YYYY-MM-DD format

- **endDate**

    end date filter on orders in YYYY-MM-DD format

- **tradeSide/side**

    - BUY
    - SELL

- **status**

    - PENDING
    - COMPLETED
    - FAILED
    - CANCELLED

- **quantity**

    quantity of BUY/SELL in real numbers like 0.22, 1.2 etc

- **requestId**

    requestId can only be fetch by requesting on endpoint 3 and then can be used before expiry time.

- **orderType**

    - MARKET_ORDER